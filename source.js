var func_obj, bTrans, bReset, prevHist;

var func = "x^2";

var parts = 20
var chunks = 16

function toLaTeX(equation){
    return equation
    return '<img src="http://latex.codecogs.com/gif.latex?' + equation.replace("(x)", "x") + '" border="0"/>';
}

function customEval(eq){
    if(eq[0]=='-') eq = eq.slice(1) + '*(-1)';
    return eval(eq.replace('^', '**'))
}

function setup() {
    let canvas = createCanvas(windowWidth*0.6, windowHeight*0.8);
    canvas.parent('plot');

    func_obj = document.getElementById('func');
    func_obj.innerHTML = toLaTeX(func);

    bTrans = document.getElementById("bTrans");
    bReset = document.getElementById("bReset");
}

function draw() {
    clear();
    background('white');

    // draw axes
    line(0, height/2, width, height/2);
    line(width/2, 0, width/2, height);
    for(let i=1; i<parts; ++i){
        line(i*width/parts, height/2-10, i*width/parts, height/2+10);   // vertical
        text((i-parts/2).toString(), i*width/parts+5, height/2+20);
        line(width/2-10, i*height/parts, width/2+10, i*height/parts);   // horizontal
        if(i-parts/2 != 0) text((parts/2-i).toString(), width/2+10, i*height/parts + 10);
    }

    //draw graph
    prevy = -customEval(func.replace('x', '('+(-parts/2).toString()+')')) * height/parts + height/2;
    for(let i=1/chunks; i<=parts; i += 1/chunks){
        let y = -customEval( func.replace('x', '('+(i-parts/2).toString()+')') ) * height/parts + height/2;
        line((i-1/chunks)*width/parts, prevy, i*width/parts, y);
        prevy = y;
   }
}

function windowResized() {
    resizeCanvas(windowWidth*0.6, windowHeight*0.8);
}

function addTrans(){
    bTrans.disabled = true;
    bReset.disabled = true;

    prevHist = document.getElementById('history').innerHTML;
    document.getElementById("history").innerHTML += 'Choose transformation:</br><button onclick="innerTrans(\'-f(x)\')">flip horizontally -f(x)</button><br/><button onclick="innerTrans(\'f(-x)\')">flip vertically f(-x)</button><br/><button onclick="innerTrans(\'fabs(x)\')">copy positive xs f(abs(x))</button><br/><button onclick="innerTrans(\'abs(f(x))\')">flip negative ys abs(f(x))</button><br/><button onclick="innerTrans(\'f(x-z)\')">shift horizontally f(x-a)</button><br/><button onclick="innerTrans(\'f(x)+z\')">shift vertically f(x)+a</button><br/>';
}

function innerTrans(eq){
    if(eq.search('z') != -1){
        eq = eq.replace('z', prompt('Write shift value', '1'));
    }

    if(eq[0]=='f' && eq.slice(-1)==')'){
        // x replacing
        func = func.replace('x', eq.slice(1, -1) + ')');
    }else{
        // inserting function
        func = eq.replace('f(x)', func);
    }

    // clear function
    func = func.replace('--', '+').replace('++', '+').replace('+-', '-').replace('-+', '-');
    if(func[0] == '+') func = func.slice(1);

    // add to history
    document.getElementById("history").innerHTML = prevHist + toLaTeX(func) + '</br>';
    func_obj.innerHTML = toLaTeX(func);

    bTrans.disabled = false;
    bReset.disabled = false;
}

function eqReset(){
    bTrans.disabled = true;
    bReset.disabled = true;

    prevHist = document.getElementById("history").innerHTML;
    document.getElementById("history").innerHTML = 'Choose basic equation:</br><button onclick="innerReset(\'x\')">f(x) = x</button><br/><button onclick="innerReset(\'(x)^2\')">f(x) = x^2</button><br/><button onclick="innerReset(\'(x)^3\')">f(x) = x^3</button><br/><button onclick="innerReset(\'1/x\')">f(x) = 1/x</button><br/><button onclick="innerReset(\'sqrt(x)\')">f(x) = sqrt(x)</button><br/><button onclick="innerReset(\'2^x\')">f(x) = 2^x</button><br/><button onclick="innerReset(\'sin(x)\')">f(x) = sin(x)</button><br/><button onclick="innerReset(\'cos(x)\')">f(x) = cos(x)</button><br/><button onclick="innerReset(\'log(x)\')">f(x) = log(x)</button><br/><button onclick="innerReset(\'back\')">back</button><br/>';
}

function innerReset(eq){
    if(eq == 'back'){
        document.getElementById("history").innerHTML = prevHist;
    }else{
        func = eq;

        document.getElementById("history").innerHTML = toLaTeX(eq) + '</br>';
        func_obj.innerHTML = toLaTeX(func);
    }

    bTrans.disabled = false;
    bReset.disabled = false;
}